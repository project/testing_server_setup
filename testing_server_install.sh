#!/bin/bash


# Install script for Drupal testing framework servers.

# Usage: ./testing_server_install.sh [ -h | --help ]

##############################################################################
# CONFIG - Don't edit unless instructed to by a drupal.org testing server
#          administrator!
##############################################################################

SCRIPT_DIR=`pwd`
SETUP_DIR="/var/local"
TESTING_INSTALL_DIR="$SETUP_DIR/testing_server_install"
TESTING_INSTALL_BRANCH=HEAD

##############################################################################
# END CONFIG
##############################################################################

usage() {
	echo "
Install script for Drupal testing framework servers.

Usage: ./testing_server_install.sh [ -h | --help ]

The script must be placed in the $SETUP_DIR directory, made executable,
and run with root privileges (either as root or using sudo).
"
}

print_error()
{
  echo -en "\033[31m$1\033[0m\n"
  echo "Please correct the issue and re-run the script."
  exit 1
}

# Always make sure these files are protected from everybody but root.
chown root $SETUP_DIR/testing_server_install.sh
chmod 744 $SETUP_DIR/testing_server_install.sh

# Check arguments.
if [ "$#" != "0" ]; then
	if [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
		usage
		exit 0
	else
		usage
		print_error "Bad arguments."
	fi
fi

# Make sure we're located in $INSTALL_SETUP_DIR.
if [ "$SCRIPT_DIR" != "$SETUP_DIR" ]; then
	print_error "The install script must be located in ${SETUP_DIR}."
fi

if [ ! -f "$TESTING_INSTALL_DIR/install" ]; then
	echo "Checking out the testing server setup files from the Drupal contributions repository."
	cvs -Q -z6 -d:pserver:anonymous:anonymous@cvs.drupal.org:/cvs/drupal-contrib checkout -d testing_server_install -r $TESTING_INSTALL_BRANCH contributions/modules/testing_server_setup/testing_server_install
	CVS_COMMAND_RETURN=$?
else
	echo "Updating the testing server setup files from the Drupal contributions repository."
	cd $TESTING_INSTALL_DIR
	cvs -Q update -dP -r $TESTING_INSTALL_BRANCH
	CVS_COMMAND_RETURN=$?
	cd $SETUP_DIR
fi

if [ "$CVS_COMMAND_RETURN" -ne "0" ]; then
	print_error "CVS does not appear to be installed, or is not working correctly."
else
	. $TESTING_INSTALL_DIR/install
fi

exit 0