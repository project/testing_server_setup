
TESTING SERVER INSTALL PACKAGE

OVERVEIW:
This package checks all server dependencies, installs the Drupal codebase and
supporting testing modules, configures PHP and an Apache virtual host for the
website, installs all necessary databases and configures access permissions
for Drupal, installs a pre-built version of Drupal, and sets up the environment
for testing admins.

SUPPORTED DISTROS:
Distro profiles are in the 'profiles' directory -- any details relevant to the
profile will be in the comments at the top of the profile.

Other distros may work fine with this script, or require some minor editing of
the server configuration variables in one of the distro profiles.

PREREQUISITES:
The following must be installed and running before attempting to run the server
install script:

  - CVS (client only)
  - An Apache webserver configured to support a Drupal installation, including
    a working mod_rewrite.
  - PHP 5.2 or greater, both the Apache module and CLI.
  - A supported Drupal 7 database.
  - A firewall set up according to the installation instructions for the Drupal
    project_issue_file_review module.

TO USE:
1. Drop 'testing_server_setup.sh' from the testing_server_setup module into
   /var/local directory on your server.
   If /var/local doesn't exist, create it and set it's permissions to 755.

   An easy way to do this is:
   a) change directory to /var
     cd /var

   b) checkout the testing_server_setup module from CVS
     cvs -z6 -d :pserver:anonymous:anonymous@cvs.drupal.org:/cvs/drupal-contrib checkout -d local contributions/modules/testing_server_setup

     To checkout a specific version of the module, use:
       cvs -z6 -d :pserver:anonymous:anonymous@cvs.drupal.org:/cvs/drupal-contrib checkout -d local -r TAG contributions/modules/testing_server_setup
     where TAG is the CVS tag required

   c) Set the permissions on /var/local
     chmod 755 /var/local


2. Execute the following commands:
     cd /var/local
     ./testing_server_install.sh

   This will download the rest of the setup files, and set up a default
   configuration file which you will need to edit according to your server's
   setup.

   NOTE: Since the config file contains sensitive information about your MySQL
         server, you should make sure the file is owned by root, and read/write
         only for root.

3. The default setup files come with administrator profiles for the currently
   recommended testing server administrators for drupal.org testing servers.
   To add more testing administration users, create new files in the 'admins'
   directory, using the same format as any of the existing admin files. Make
   sure the name of any file you create matches the admin user's desired login
   name.

   NOTE: If you wish to restrict the allowed admins for the server, edit the
         ALLOWED_ADMINS setting in the configuration file to suit your needs.

4. Re-run the commands in Step 2 to complete the installation.

5. Ensure that the db information in the settings.php file is correct. Read the
   INSTALL.txt in the Project issue file review (PIFR) module at step #4 for
   details.

6. Perform step #5 of the INSTALL.txt in the Project issue file review (PIFR)
   module.

If any of the prerequisite tests fail, correct the issue and re-run the script.
If any serious errors occur in the install script, you'll need to contact a
testing server administrator to help resolve the issue.
